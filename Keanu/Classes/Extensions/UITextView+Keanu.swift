//
//  UITextView+Keanu.swift
//  Keanu
//
//  Created by N-Pex on 30.12.19.
//

import UIKit

public extension UITextView {
    
    @IBInspectable
    var textInsets: String
    {
        set(value) {
            let val = value.split(separator: ",")
            if val.count == 4 {
                let values = val.map { (s) -> CGFloat in
                    return CGFloat(Float(string: String(s)) ?? 0.0)
                }
                textContainerInset = UIEdgeInsets(top: values[0], left: values[1], bottom: values[2], right: values[3])

            }
        }
        
        get {
            let insets = textContainerInset
            return "\(insets.top),\(insets.left),\(insets.bottom),\(insets.right)"
        }
    }
}
