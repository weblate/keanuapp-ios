//
//  UIStoryboard+Keanu.swift
//  Keanu
//
//  Created by N-Pex on 29.01.19.
//  Copyright © 2019 Guardian Project. All rights reserved.
//

import UIKit

extension UIStoryboard {
    /**
     Tries to load a storyboard, first from main bundle. If not found, load from
     current bundle. This allows users of this pod to override storyboards in
     their apps.
     */
    public class func overriddenOrLocal(name: String) -> UIStoryboard {
        for bundle in Bundle.allBundles {
            // Note: look for compiled storyboard, hence "storyboardc"
            if let _ = bundle.path(forResource: name, ofType: "storyboardc") {
                return UIStoryboard(name: name, bundle: bundle)
            }
        }

        return UIStoryboard(name: name, bundle: Bundle(for: BaseAppDelegate.self))
    }
    
    /**
     Same as "instantiateInitialViewController", but this will also call a delegate
     func on the AppDelegate, so apps can perform additional step(s) when a new
     view controller has been created.
     */
    public func instantiateInitialVCWithDelegate() -> UIViewController? {
        let result = instantiateInitialViewController()

        if let delegate = UIApplication.shared.delegate as? BaseAppDelegate {
            delegate.storyboard(self, instantiatedInitialViewController: result)
        }

        return result
    }
    
    /**
     Same as "instantiateViewControllerWithIdentifier", but this will also call
     a delegate func on the AppDelegate, so apps can perform additional step(s)
     when a new view controller has been created.
     */
    public func instantiateVCWithDelegate(withIdentifier identifier: String) -> UIViewController? {
        let result = instantiateViewController(withIdentifier: identifier)

        if let delegate = UIApplication.shared.delegate as? BaseAppDelegate {
            delegate.storyboard(self, instantiatedViewController: result, identifier: identifier)
        }

        return result
    }
}
