//
//  MXAggregatedReactions+Keanu.swift
//  Keanu
//
//  Created by N-Pex on 07.05.20.
//

import KeanuCore
import MatrixSDK

extension MXAggregatedReactions {
    public func withSingleEmoji() -> MXAggregatedReactions? {
        let filtered = self.reactions.filter { (reactionCount) -> Bool in
            return MXKTools.isSingleEmojiString(reactionCount.reaction)
        }
        guard filtered.count > 0 else { return nil }
        let singleEmojis = MXAggregatedReactions()
        singleEmojis.reactions = filtered
        return singleEmojis
    }
}
