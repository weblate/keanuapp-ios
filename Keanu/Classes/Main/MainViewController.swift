//
//  MainViewController.swift
//  Keanu
//
//  Created by Benjamin Erhart on 16.03.20.
//

import UIKit

open class MainViewController: UITabBarController {

    override open func viewDidLoad() {
        super.viewDidLoad()

        let router = UIApplication.shared.router

        viewControllers = [
            item(router.chatList(), "ic_message_white", 1),
            item(router.chooseFriends(), "ic_group_white", 2),
            item(router.discover(), "ic_discover_white", 3),
            item(router.me(), "ic_face_white", 4)]
    }

    /**
     Create a `UINavigationController` with the given root view controller using the given named image
     as `UITabBarItem` and with the given tag.

     - parameter vc: The root view controller.
     - parameter image: The image name to use for the `UITabBarItem`.
     - parameter tag: The tag ID to use for the `UITabBarItem`. (Typically not important but it should be some unique number.
     - returns: a `UINavigationController` containing the `vc` as root view controller.
     */
    open func item(_ vc: UIViewController, _ image: String, _ tag: Int) -> UINavigationController {
        let navC = UINavigationController(rootViewController: vc)

        navC.tabBarItem = UITabBarItem(
            title: nil,
            image: UIImage(named: image, in: Bundle(for: type(of: self)), compatibleWith: nil),
            tag: tag)

        return navC
    }

    @discardableResult
    open func showChatList() -> UINavigationController? {
        return show(ChatListViewController.self)
    }

    @discardableResult
    open func showChooseFriends() -> UINavigationController? {
        return show(ChooseFriendsViewController.self)
    }

    @discardableResult
    open func showDiscover() -> UINavigationController? {
        return show(DiscoverViewController.self)
    }

    @discardableResult
    open func showMe() -> UINavigationController? {
        return show(MeViewController.self)
    }

    open func show(_ viewControllerType: UIViewController.Type) -> UINavigationController? {
        var index = 0

        for vc in viewControllers ?? [] {
            if type(of: vc) == viewControllerType {
                show(index)

                return nil
            }

            if let navC = vc as? UINavigationController {
                for vc in navC.viewControllers {
                    if type(of: vc) == viewControllerType {
                        show(index)
                        navC.popToViewController(vc, animated: false)

                        return navC
                    }
                }
            }

            index += 1
        }

        return nil
    }

    open func show(_ selectedIndex: Int) {
        // If we are presenting, close the presented VC first.
        if let presented = presentedViewController {
            presented.dismiss(animated: false)
        }

        self.selectedIndex = selectedIndex
    }
}
