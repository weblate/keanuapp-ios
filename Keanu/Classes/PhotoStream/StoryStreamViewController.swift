//
//  StoryStreamViewController.swift
//  Keanu
//
//  Created by N-Pex on 16.4.20.
//  Copyright © 2020 Guardian Project. All rights reserved.
//

import UIKit
import INSPhotoGallery
import MatrixSDK
import KeanuCore
import QuickLook

open class StoryStreamViewController: UITableViewController, StreamDataSourceDelegate {
    public weak var room: MXRoom?
    
    /**
     Optional event to show when data source is loaded.
     */
    public weak var initialEvent: MXEvent?

    private lazy var workingOverlay: WorkingOverlay = {
        return WorkingOverlay().addToSuperview(view)
    }()

    private var streamDataSource:StreamDataSource?
    
    /**
     Use a "MessageTableView" instead of the regular UITableView.
     */
    open override var tableView: UITableView! {
        get {
            if !(super.tableView is MessageTableView) {
                // Replace the original table view with a MessageTableView!
                super.tableView = MessageTableView(frame: super.tableView.frame)
                super.tableView.backgroundColor = .keanuBackground
            }
            return super.tableView
        }
        set(value) {
            super.tableView = value
        }
    }

    /**
     Convenience getter for message table view.
     */
    open var messageTableView: MessageTableView? {
        get {
            return self.tableView as? MessageTableView
        }
    }
    
    open override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = "Story Stream".localize()

        workingOverlay.message = "Loading... tap to cancel".localize()
        workingOverlay.tapHandler = { [weak self] in
            guard let self = self else {return}
            self.workingOverlay.isHidden = true
            self.streamDataSource?.invalidate()
            self.streamDataSource = nil
            self.navigationController?.popViewController(animated: true)
        }
        workingOverlay.isHidden = false
        
        tableView.register(StoryWebViewCell.self, forCellReuseIdentifier: "storyCell")
        
        streamDataSource = StreamDataSource(room: room)
        streamDataSource?.delegate = self
    }
    
    @IBAction func done() {
        navigationController?.dismiss(animated: true)
    }
    
    // MARK: StreamDataSourceDelegate
    func didUpdateProgress(source: StreamDataSource) {
        guard !source.initialized else {return} // Only when loading
        if source.progressTotal > 0 {
            if tableView.backgroundView == nil {
                tableView.backgroundView = UIProgressView(progressViewStyle: .bar)
            }
            if let progress = tableView.backgroundView as? UIProgressView {
                progress.setProgress(Float(source.progressDone) / Float(source.progressTotal), animated: true)
            }
        } else {
            tableView.backgroundView = nil
        }
    }
    
    func didInitialize(source: StreamDataSource) {
        tableView.backgroundView = nil // Hide progress
        //self.noPhotosView.isHidden = images.count > 0
        self.workingOverlay.isHidden = true
        self.tableView.reloadData()
        if let initialEvent = self.initialEvent,
            let index = source.indexOf(event: initialEvent) {
            self.tableView.scrollToRow(at: IndexPath(row: index, section: 0), at: .top, animated: false)
        }
    }
    
    func shouldInclude(source: StreamDataSource, event: MXEvent) -> Bool {
        return event.isHtmlAttachment
    }
    
    func didChange(source: StreamDataSource, deleted: [IndexPath]?, added: [IndexPath]?, changed: [IndexPath]?, live: Bool, disableScroll: Bool) {
        messageTableView?.didChange(deleted: deleted, added: added, changed: changed, live: false, disableScroll: true)
    }
    
    open override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    open override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let ds = streamDataSource, ds.initialized else {return 0}
        return ds.events.count
    }
    
    open override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let event:StreamEvent? = streamDataSource?.events[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "storyCell", for: indexPath)
        if
            let cell = cell as? StoryWebViewCell,
            let event = event,
            let e = event.event,
            let mm = event.room?.mxSession?.mediaManager {
            cell.setStory(e, mediaManager: mm)
        }
        return cell
    }
    
    open override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let event = streamDataSource?.events[indexPath.row]
        if
            let event = event,
            let e = event.event,
            let mm = event.room?.mxSession?.mediaManager,
            let attachment = MXKAttachment(event: e, andMediaManager: mm)
        {
            let _ = AttachmentItem(attachment, event: e) { [weak self] item in
                guard let self = self else {return}
                if #available(iOS 11.0, *) {
                    self.present(AttachmentViewController.instantiate(item), animated: true)
                }
            }
        }
    }
}
