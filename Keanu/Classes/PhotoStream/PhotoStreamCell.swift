//
//  PhotoStreamCell.swift
//  Keanu
//
//  Created by N-Pex on 18.01.19.
//  Copyright © 2019 Guardian Project. All rights reserved.
//

import UIKit

open class PhotoStreamCell: UICollectionViewCell {

    open class var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }

    open class var defaultReuseId: String {
        return String(describing: self)
    }


    @IBOutlet weak var imageView:UIImageView!

    private var photo: PhotoStreamImage?
    
    override open func prepareForReuse() {
        super.prepareForReuse()

        imageView.image = nil

        photo?.releaseImages()
        photo = nil
    }
    
    open func populateWithPhoto(_ photo: PhotoStreamImage) {
        self.photo = photo

        photo.loadThumbnailImageWithSizeAndCompletionHandler(imageView.bounds.size)
        { image, error in
            self.imageView.image = image
        }
    }
}
