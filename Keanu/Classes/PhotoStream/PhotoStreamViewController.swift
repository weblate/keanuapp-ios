//
//  PhotoStreamViewController.swift
//  Keanu
//
//  Created by N-Pex on 18.0.19.
//  Copyright © 2019 Guardian Project. All rights reserved.
//

import UIKit
import INSPhotoGallery
import MatrixSDK

open class PhotoStreamViewController: UIViewController, UICollectionViewDelegate,
UICollectionViewDataSource, PhotosViewControllerDelegate {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var noPhotosView: UILabel!

    public var room: MXRoom?

    private lazy var workingOverlay: WorkingOverlay = {
        return WorkingOverlay().addToSuperview(view)
    }()
    
    private var assetGridThumbnailSize = CGSize()
    private lazy var photoStreamHandler = PhotoStreamHandler()

    open override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = "Photo Stream".localize()
        navigationItem.rightBarButtonItem = UIBarButtonItem(
            barButtonSystemItem: .done, target: self, action: #selector(done))

        noPhotosView.isHidden = true

        collectionView.register(PhotoStreamCell.nib, forCellWithReuseIdentifier: PhotoStreamCell.defaultReuseId)

        photoStreamHandler.fetchImagesAsync(room: room, initialEvent: nil, onStart: {
            self.workingOverlay.message = "Loading... tap to cancel".localize()
            self.workingOverlay.tapHandler = {
                self.workingOverlay.isHidden = true
                self.photoStreamHandler.cancelFetching()
                self.navigationController?.popViewController(animated: true)
            }
            self.workingOverlay.isHidden = false
        }, onEnd: { images, initialImage in
            self.noPhotosView.isHidden = images.count > 0
            self.workingOverlay.isHidden = true
            self.collectionView?.reloadData()
        })
    }

    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Determine the size of the thumbnails to request from the PHCachingImageManager
        let scale = UIScreen.main.scale
        let cellSize = (collectionView.collectionViewLayout as! UICollectionViewFlowLayout).itemSize;
        assetGridThumbnailSize = CGSize(width: cellSize.width * scale, height: cellSize.height * scale);
    }


    // Mark - UICollectionViewDataSource

    open func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photoStreamHandler.images.count
    }
    
    open func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier:
            PhotoStreamCell.defaultReuseId, for: indexPath)

        if let cell = cell as? PhotoStreamCell {
            cell.populateWithPhoto(photoStreamHandler.images[indexPath.item])
        }

        return cell
    }
    
    open func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = self.collectionView?.cellForItem(at: indexPath) as! PhotoStreamCell

        photoStreamHandler.initialImage = photoStreamHandler.images[indexPath.item]

        let browser = PhotosViewController(photoStreamHandler: photoStreamHandler, referenceView: cell.imageView)
        browser.delegate = self
        browser.referenceViewForPhotoWhenDismissingHandler = { photo in
            if let index = self.photoStreamHandler.images.firstIndex(where: {$0 === photo}) {
                return (collectionView.cellForItem(at: IndexPath(item: index, section: 0))
                    as? PhotoStreamCell)?.imageView
            }

            return nil
        }

        present(browser, animated: true)
    }
    
    public func didDeletePhoto(photo: PhotoStreamImage) {
        if let index = photoStreamHandler.images.firstIndex(where: {$0 === photo}) {
            photoStreamHandler.images.remove(at: index)
            collectionView?.deleteItems(at: [IndexPath(item: index, section: 0)])
        }
    }
    
    @IBAction func done() {
        navigationController?.dismiss(animated: true)
    }
}
