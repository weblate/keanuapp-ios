//
//  BaseRouter.swift
//  Keanu
//
//  Created by N-Pex on 06.02.20.
//  Copyright © 2020 Guardian Project. All rights reserved.
//

open class BaseRouter: NSObject, Router {

    /**
     Singleton instance.
     */
    public static let shared = BaseRouter()

    open func welcome() -> WelcomeViewController {
        return WelcomeViewController(nibName: String(describing: WelcomeViewController.self),
                                     bundle: Bundle(for: type(of: self)))
    }

    open func addAccount() -> AddAccountViewController {
        return AddAccountViewController(nibName: String(describing: AddAccountViewController.self),
                                        bundle: Bundle(for: type(of: self)))
    }

    open func enablePush() -> EnablePushViewController {
        return EnablePushViewController(nibName: String(describing: EnablePushViewController.self),
                                        bundle: Bundle(for: type(of: self)))
    }
    
    public func main() -> MainViewController {
        return MainViewController()
    }

    public func chatList() -> ChatListViewController {
        return ChatListViewController(nibName: String(describing: ChatListViewController.self),
                                bundle: Bundle(for: type(of: self)))
    }

    public func discover() -> DiscoverViewController {
        return DiscoverViewController()
    }

    public func me() -> MeViewController {
        return MeViewController()
    }

    public func myProfile() -> MyProfileViewController {
        return MyProfileViewController()
    }
    
    open func room() -> RoomViewController {
        let vc = RoomViewController()
        vc.hidesBottomBarWhenPushed = true
        return vc
    }

    public func roomSettings() -> RoomSettingsViewController {
        return RoomSettingsViewController(nibName: String(describing: RoomSettingsViewController.self),
                                          bundle: Bundle(for: type(of: self)))
    }

    open func profile() -> ProfileViewController {
        return ProfileViewController()
    }

    public func story() -> StoryViewController {
        return StoryViewController(nibName: String(describing: StoryViewController.self),
                                   bundle: Bundle(for: type(of: self)))
    }

    public func storyAddMedia() -> StoryAddMediaViewController {
        return StoryAddMediaViewController(nibName: String(describing: StoryAddMediaViewController.self),
                                   bundle: Bundle(for: type(of: self)))
    }

    public func storyGallery() -> StoryGalleryViewController {
        return StoryGalleryViewController(nibName: String(describing: StoryGalleryViewController.self),
                                   bundle: Bundle(for: type(of: self)))
    }

    public func storyEditor() -> StoryEditorViewController {
        return StoryEditorViewController(nibName: String(describing: StoryEditorViewController.self),
                                         bundle: Bundle(for: type(of: self)))
    }

    public func stickerPack() -> StickerPackViewController {
        return StickerPackViewController()
    }

    public func pickSticker() -> PickStickerViewController {
        return PickStickerViewController(nibName: String(describing: PickStickerViewController.self),
                                         bundle: Bundle(for: type(of: self)))
    }

    public func chooseFriends() -> ChooseFriendsViewController {
        return ChooseFriendsViewController(nibName: String(describing: ChooseFriendsViewController.self),
                                           bundle: Bundle(for: type(of: self)))
    }

    public func addFriend() -> AddFriendViewController {
        return AddFriendViewController(nibName: String(describing: AddFriendViewController.self),
                                       bundle: Bundle(for: type(of: self)))
    }

    public func showQr() -> ShowQrViewController {
        return ShowQrViewController(nibName: String(describing: ShowQrViewController.self),
                                    bundle: Bundle(for: type(of: self)))
    }

    public func qrScan() -> QrScanViewController {
        return QrScanViewController(nibName: String(describing: QrScanViewController.self),
                                    bundle: Bundle(for: type(of: self)))
    }

    public func newDevice() -> NewDeviceViewController {
        return NewDeviceViewController(nibName: String(describing: NewDeviceViewController.self),
                                       bundle: Bundle(for: type(of: self)))
    }

    public func manualCompare() -> ManualCompareViewController {
        return ManualCompareViewController(nibName: String(describing: ManualCompareViewController.self),
                                           bundle: Bundle(for: type(of: self)))
    }

    public func intrusion() -> IntrusionViewController {
        return IntrusionViewController(nibName: String(describing: IntrusionViewController.self),
                                       bundle: Bundle(for: type(of: self)))
    }

    public func verification() -> VerificationViewController {
        return VerificationViewController(nibName: String(describing: VerificationViewController.self),
                                          bundle: Bundle(for: type(of: self)))
    }

    public func photoStream() -> PhotoStreamViewController {
        return PhotoStreamViewController(nibName: String(describing: PhotoStreamViewController.self),
                                         bundle: Bundle(for: type(of: self)))
    }
    
    public func chatBubble(type: BubbleViewType, rect: CGRect) -> CGPath? {
        return BubbleLayer.createPath(
            type: type, frame: rect,
            lineWidth: [BubbleViewType.incoming, BubbleViewType.outgoing].contains(type) ? 2 : 0)
    }
}
