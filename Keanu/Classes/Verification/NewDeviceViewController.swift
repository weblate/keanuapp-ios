//
//  NewDeviceViewController.swift
//  Keanu
//
//  Created by Benjamin Erhart on 28.07.20.
//

import KeanuCore

open class NewDeviceViewController: UIViewController {

    open var session: MXSession?
    open var device: MXDeviceInfo?

    @IBOutlet weak var illustrationLb: UILabel! {
        didSet {
            illustrationLb.font = .materialIcons(ofSize: 84)
            illustrationLb.text = .devices
        }
    }

    @IBOutlet weak var deviceNameLb: UILabel!

    @IBOutlet weak var deviceIdLb: UILabel!

    @IBOutlet weak var titleLb: UILabel! {
        didSet {
            titleLb.text = "Is this your device?".localize()
        }
    }

    @IBOutlet weak var messageLb: UILabel! {
        didSet {
            messageLb.text = "If you don't recognize this device, your messages might be accessed by strangers without your permission."
                .localize()
        }
    }

    @IBOutlet weak var noBt: UIButton! {
        didSet {
            noBt.setTitle("No".localize())
        }
    }

    @IBOutlet weak var yesBt: UIButton! {
        didSet {
            yesBt.setTitle("Yes".localize())
        }
    }

    open override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.leftBarButtonItem = UIBarButtonItem(
            barButtonSystemItem: .cancel, target: self, action: #selector(cancel))
        navigationItem.title = "New Log In".localize()

        let deviceName = device?.displayName ?? "–"

        deviceNameLb.attributedText = NSAttributedString.withBoldLabel(
            "Name: %".localize(value: deviceName),
            onlyContent: deviceName,
            size: deviceNameLb.font.pointSize)

        let deviceId = device?.deviceId ?? "–"

        deviceIdLb.attributedText = NSAttributedString.withBoldLabel(
            "Identifier: %".localize(value: deviceId),
            onlyContent: deviceId,
            size: deviceIdLb.font.pointSize)
    }


    // MARK: Actions

    @IBAction func no() {
        let intrusionVc = UIApplication.shared.router.intrusion()
        intrusionVc.matrixId = session?.myUserId

        navigationController?.pushViewController(intrusionVc, animated: true)
    }

    @IBAction func yes() {
        guard let session = session, let device = device else {
            return
        }

        VerificationManager.shared.interactiveVerify(session, device) { error in
            print("[\(String(describing: type(of: self)))] error=error")
        }
    }

    @objc func cancel() {
        if let navC = navigationController {
            navC.dismiss(animated: true)
        }
        else {
            dismiss(animated: true)
        }
    }
}
