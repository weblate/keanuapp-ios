//
//  ChatListProtocol.swift
//  Keanu
//

import UIKit
import MatrixSDK

public protocol ChatListProtocol: UIViewController, ChooseFriendsDelegate {
    func openRoom(_ room: MXRoom?)
    func createEmptyRoom(session: MXSession)
}
