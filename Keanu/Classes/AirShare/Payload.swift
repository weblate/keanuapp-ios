//
//  Payload.swift
//  Keanu
//
//  Created by Benjamin Erhart on 30.04.20.
//

import Foundation

struct Payload: Codable {

    let invite: Invite?
}

struct Invite: Codable {

    let roomAlias: String
}
