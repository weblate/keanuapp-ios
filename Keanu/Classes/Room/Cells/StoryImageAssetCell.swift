//
//  StoryImageAssetCell.swift
//  Keanu
//
//  Created by Benjamin Erhart on 05.03.20.
//

import UIKit

class StoryImageAssetCell: UICollectionViewCell {

    open class var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }

    open class var defaultReuseId: String {
        return String(describing: self)
    }


    @IBOutlet weak var imageView: UIImageView!
}
