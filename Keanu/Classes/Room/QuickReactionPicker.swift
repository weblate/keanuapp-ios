//
//  QuickReactionPicker.swift
//  Keanu
//
//  Created by N-Pex on 08.05.20.
//

import UIKit
import ISEmojiView

open class QuickReactionPicker: UIView, ISEmojiView.EmojiViewDelegate, UIGestureRecognizerDelegate {

    var didPickEmojiCallback: ((String) -> ())? = nil
    
    open var emojiPicker: ISEmojiView.EmojiView?
    open var emojiPickerBottomConstraint: NSLayoutConstraint?
    open var emojiPickerCloseTapRecognizer: UITapGestureRecognizer?
    
    open class func showIn(view: UIView, didPickEmoji:@escaping (String) -> ()) {
        let picker = QuickReactionPicker()
        picker.showInView(view, didPickEmoji: didPickEmoji)
    }
    
    open func showInView(_ view: UIView, didPickEmoji:@escaping (String) -> ()) {
        self.didPickEmojiCallback = didPickEmoji
        
        // First add ourselves to parent view
        backgroundColor = UIColor(white: 0, alpha: 0.5)
        translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(self)
        autoPinEdgesToSuperviewEdges()

        emojiPickerCloseTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(closeEmojiPicker))
        if let recognizer = emojiPickerCloseTapRecognizer {
            recognizer.delegate = self
            self.addGestureRecognizer(recognizer)
        }
        
        // Then add emoji view to ourselves
        let keyboardSettings = KeyboardSettings(bottomType: .categories)
        let emojiView = ISEmojiView.EmojiView(keyboardSettings: keyboardSettings)
        emojiView.translatesAutoresizingMaskIntoConstraints = false
        emojiView.delegate = self
        self.addSubview(emojiView)
        
        emojiPickerBottomConstraint = emojiView.autoPinEdge(toSuperviewEdge: .bottom, withInset: 0)
        emojiView.autoPinEdge(toSuperviewEdge: .leading)
        emojiView.autoPinEdge(toSuperviewEdge: .trailing)
        emojiView.autoMatch(.height, to: .height, of: self, withMultiplier: 0.3)
        
        self.emojiPicker = emojiView
        self.emojiPickerCloseTapRecognizer?.isEnabled = true
        emojiView.layoutIfNeeded()
        emojiPickerBottomConstraint?.constant = emojiView.frame.height
        layoutIfNeeded()
        DispatchQueue.main.async {
            self.emojiPickerBottomConstraint?.constant = 0
            UIView.animate(withDuration: 0.3, animations: { [weak self] in
                self?.layoutIfNeeded()
            })
        }
    }

    @objc public func closeEmojiPicker() {
        guard let emojiPicker = self.emojiPicker else {return}
        UIView.animate(withDuration: 0.3, animations: { [weak self] in
            self?.emojiPickerBottomConstraint?.constant = emojiPicker.frame.height
            self?.layoutIfNeeded()
        }) { [weak self] _ in
            self?.emojiPickerCloseTapRecognizer?.isEnabled = false
            self?.emojiPicker?.removeFromSuperview()
            self?.emojiPicker = nil
            self?.removeFromSuperview()
        }
    }
    
    public func emojiViewDidSelectEmoji(_ emoji: String, emojiView: ISEmojiView.EmojiView) {
        didPickEmojiCallback?(emoji)
        closeEmojiPicker()
    }
    
    open override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if gestureRecognizer == emojiPickerCloseTapRecognizer {
            // Close only on background tap, not when tapping anything else.
            let point = gestureRecognizer.location(in: self)
            if let viewTouched = self.hitTest(point, with: nil), viewTouched != self {
                return false
            } else {
                return true
            }
        }
        return super.gestureRecognizerShouldBegin(gestureRecognizer)
    }
}
