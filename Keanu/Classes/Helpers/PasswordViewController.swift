//
//  PasswordViewController.swift
//  Keanu
//
//  Created by Benjamin Erhart on 22.06.20.
//

import UIKit
import Eureka

open class PasswordViewController: FormViewController {

    public enum Style {
        case entry
        case confirm
        case newPassword
    }

    open class func instantiate(style: Style = .entry, title: String? = nil, message: String? = nil,
                                cancelLabel: String? = nil, okLabel: String? = nil,
                                completion: ((PasswordViewController) -> Void)? = nil) -> UINavigationController
    {
        let vc = PasswordViewController()
        vc.style = style
        vc.title = title
        vc.message = message
        vc.cancelLabel = cancelLabel
        vc.okLabel = okLabel
        vc.completion = completion

        return UINavigationController(rootViewController: vc)
    }

    open private(set) var style: Style = .entry
    open private(set) var message: String?
    open private(set) var cancelLabel: String?
    open private(set) var okLabel: String?
    open private(set) var completion: ((PasswordViewController) -> Void)?


    open var oldPassword: String? {
        if form.validate().isEmpty {
            return (form.rowBy(tag: "old_password") as? PasswordRow)?.value
        }

        return nil
    }

    open var password: String? {
        if form.validate().isEmpty,
            let password = (form.rowBy(tag: "password") as? PasswordRow)?.value,
            !password.isEmpty
        {
            return password
        }

        return nil
    }

    open override func viewDidLoad() {
        super.viewDidLoad()

        form
        +++ Section(message)

        if style == .newPassword {
            form.last!
                <<< PasswordRow("old_password") {
                    $0.placeholder = "Old Password".localize()
                    $0.add(rule: RuleRequired())
                }
                .cellSetup() { cell, row in
                    cell.accessoryType = .detailButton
                }
                .onRowValidationChanged(checkRowValidity)
        }

        form.last!
            <<< PasswordRow("password") {
                $0.placeholder = "Password".localize()
                $0.add(rule: RuleRequired())

                if style != .entry {
                    $0.add(rule: RuleMinLength(minLength: 8))
                }
            }
            .cellSetup() { cell, row in
                cell.accessoryType = .detailButton
            }
            .onRowValidationChanged(checkRowValidity)

        if style == .confirm || style == .newPassword {
            form.last!
                <<< PasswordRow("confirm") {
                    $0.placeholder = "Confirm".localize()
                    $0.add(rule: RuleRequired())
                    $0.add(rule: RuleEqualsToRow(form: form, tag: "password"))
                }
                .cellSetup() { cell, row in
                    cell.accessoryType = .detailButton
                }
                .onRowValidationChanged(checkRowValidity)
        }

        form
            +++ ButtonRow() {
                $0.title = okLabel ?? "OK".localize()
                $0.disabled = true
                $0.disabled = Condition.function(["old_password", "password", "confirm"],
                                                 { !$0.validate(quietly: true).isEmpty })
            }
            .onCellSelection { [weak self] _, row in
                guard !row.isDisabled else {
                    return
                }

                if self?.form.validate().isEmpty ?? false {
                    self?.close()
                }
            }


        if let cancelLabel = cancelLabel {
            navigationItem.leftBarButtonItem = UIBarButtonItem(
                title: cancelLabel, style: .plain, target: self, action: #selector(close))
        }
        else {
            navigationItem.leftBarButtonItem = UIBarButtonItem(
                barButtonSystemItem: .cancel, target: self, action: #selector(close))
        }
    }

    open func checkRowValidity(cell: BaseCell, row: BaseRow) {
        if #available(iOS 13.0, *) {
            // Handle dark mode
            cell.backgroundColor = row.isValid ? .secondarySystemGroupedBackground : .orange
        }
        else {
            cell.backgroundColor = row.isValid ? .white : .orange
        }
    }


    // MARK: Actions

    @objc open func close() {
        if let navC = navigationController {
            navC.dismiss(animated: true) {
                self.completion?(self)
            }
        }
        else {
            dismiss(animated: true) {
                self.completion?(self)
            }
        }
    }


    // MARK: UITableViewDelegate

    /**
    Workaround to avoid capitalization of header.
    */
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if section == 0,
            let header = view as? UITableViewHeaderFooterView
        {
            header.textLabel?.text = message
        }
    }

    /**
     Toggles password visibility on tap.
     */
    func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? PasswordCell {
            cell.textField.isSecureTextEntry = !cell.textField.isSecureTextEntry
        }
    }
}
