//
//  KeyBackupCell.swift
//  Keanu
//
//  Created by Benjamin Erhart on 30.03.20.
//

import KeanuCore
import MatrixSDK

/**
 Implements the user control of device keys backup to the Matrix server.
 Private device keys will be encrypted and backup up to the server and can be restored from there with a
 new device, as long as the user remembers the password, with which they encrypted.
 */
class KeyBackupCell: UITableViewCell {

    open class var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }

    open class var defaultReuseId: String {
        return String(describing: self)
    }

    public static var height: CGFloat = 160

    @IBOutlet weak var statusLb: UILabel!
    @IBOutlet weak var enableBt: UIButton!

    private weak var session: MXSession?
    private weak var keyBackup: MXKeyBackup?

    private var enabled: Bool?

    private var vc: UIViewController? {
        return UIApplication.shared.keyWindow?.rootViewController?.top
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        registerObserver()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)

        registerObserver()
    }

    func apply(_ session: MXSession?) -> KeyBackupCell {
        self.session = session
        keyBackup = session?.crypto?.backup

        updateUi()

        return self
    }


    @IBAction func enableBackup() {
        guard let enabled = enabled,
            let session = session,
            let vc = vc else {
                return
        }


        if enabled {
            guard let version = keyBackup?.keyBackupVersion?.version else {
                return
            }

            AlertHelper.present(vc, message: "Really delete the key backup on the server?".localize(),
                                title: "Delete Key Backup".localize(),
                                actions: [
                                    AlertHelper.cancelAction(),
                                    AlertHelper.destructiveAction("Delete Key Backup".localize(), handler:
                                        { [weak self] _ in
                                            self?.delete(version)
                                    })])
        }
        else {
            vc.present(PasswordViewController.instantiate(
                style: .confirm,
                title: "Recovery Password".localize(),
                message: "Set a password for the backup which you can use to restore later:".localize()
            ) { passwordVc in
                guard let password = passwordVc.password else {
                    return
                }

                VerificationManager.shared.addNewBackup(session, password, vc: vc) { error in
                    if let error = error {
                        self.failure(error)
                    }
                }
            }, animated: true)
        }
    }

    @IBAction func restore() {
        vc?.present(PasswordViewController.instantiate(
            title: "Backup Password".localize(),
            okLabel: "Restore".localize()) { [weak self] vc in
                guard let session = self?.session,
                    let password = vc.password else {
                    return
                }

                VerificationManager.shared.restoreBackup(session, password) { error, foundKeys, importedKeys in
                    if let error = error {
                        self?.failure(error)
                        return
                    }

                    guard let vc = self?.vc else {
                        return
                    }

                    AlertHelper.present(
                        vc,
                        message: "% keys were found, % where imported!".localize(
                            values: Formatters.format(int: Int(foundKeys ?? 0)),
                            Formatters.format(int: Int(importedKeys ?? 0))),
                        title: "Restore Successful".localize())
                }
        }, animated: true)
    }


    // MARK: Private Methods

    private func registerObserver() {
        NotificationCenter.default.addObserver(
            self, selector: #selector(updateUi), name: .mxKeyBackupDidStateChange,
            object: nil)
    }

    @objc private func updateUi() {
        let callback: ((MXKeyBackupVersionTrust?) -> Void) = { [weak self] trust in

            switch self?.keyBackup?.state {

            case MXKeyBackupStateWrongBackUpVersion:
                self?.statusLb.text = "Newer version on server!".localize()
                self?.statusLb.textColor = .systemRed
                self?.enabled = false

            case MXKeyBackupStateDisabled:
                self?.statusLb.text = "Disabled".localize()
                self?.statusLb.textColor = .systemRed
                self?.enabled = false

            case MXKeyBackupStateNotTrusted:
                self?.statusLb.text = "Disabled, because backup on server is untrusted!".localize()
                self?.statusLb.textColor = .systemRed
                self?.enabled = false

            case MXKeyBackupStateEnabling,
                 MXKeyBackupStateReadyToBackUp,
                 MXKeyBackupStateWillBackUp,
                 MXKeyBackupStateBackingUp:
                self?.statusLb.text = "enabled".localize()
                self?.statusLb.textColor = .systemGreen
                self?.enabled = true

            default:
                self?.statusLb.text = "unknown".localize()
                self?.statusLb.textColor = .keanuLabel
                self?.enabled = nil
            }


            switch self?.enabled {
            case .some(true):
                self?.enableBt.setTitle("Delete Key Backup".localize())
                self?.enableBt.setTitleColor(.systemRed)
                self?.enableBt.isEnabled = true

            case .some(false):
                self?.enableBt.setTitle("Enable Key Backup".localize())
                self?.enableBt.setTitleColor(nil)
                self?.enableBt.isEnabled = true

            case .none:
                self?.enableBt.setTitle("Enable Key Backup".localize())
                self?.enableBt.setTitleColor(nil)
                self?.enableBt.isEnabled = false
            }
        }

        if let version = keyBackup?.keyBackupVersion {
            keyBackup?.trust(for: version, onComplete: callback)
        }
        else {
            callback(nil)
        }
    }

    private func delete(_ version: String) {
        keyBackup?.deleteVersion(version, success: { [weak self] in
            guard let matrixId = self?.session?.myUserId else {
                return
            }

            do {
                try Keychain.delete(Keychain.Credentials(matrixId: matrixId))
                print("[\(String(describing: type(of: self)))]#delete: Recovery key deleted!")
            }
            catch {
                print("[\(String(describing: type(of: self)))]#delete: Recovery key deletion error=\(error)")
            }
        }, failure: failure)
    }

    private func failure(_ error: Error) {
        print("[\(String(describing: type(of: self)))] error=\(error)")

        if let vc = vc {
            AlertHelper.present(vc, message: error.localizedDescription)
        }
    }
}
