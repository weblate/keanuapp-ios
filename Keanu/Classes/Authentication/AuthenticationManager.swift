//
//  AuthenticationManager.swift
//  Keanu
//
//  Created by Benjamin Erhart on 14.06.21.
//  Copyright © 2021 Guardian Project. All rights reserved.
//

import KeanuCore
import AFNetworking

public protocol AuthenticationManagerDelegate {

    /**
     Show the user, that a network request is going on in the background. Might be a good idea to show an
     overlay, so the user doesn't change anything in the meantime.
     */
    func showWorking(_ toggle: Bool)

    /**
     The server needs a Recaptcha secret and/or the terms accepted by the user. Do that and call
     `#authenticate` again, when done.
     */
    func showRecaptchaAndTerms(_ homeServer: String, _ recaptchaKey: String?, _ terms: MXLoginTerms?)

    /**
     If the `toggle` is true, an error happened which should be displayed to the user.

     If the `toggle` is false, then any errors should be hidden now, since we're beginning a new operation.
     */
    func showError(_ error: String?, _ toggle: Bool)

    /**
     Registration and/or log in was successful with the last given `username`. We're done here.
     */
    func didLogIn()
}

/**
 Handles registration or log in negotiation with the server.
 */
open class AuthenticationManager {


    /**
     Authentication types
     */
    public enum AuthType: UInt32 {

        /**
         Type used to sign up.
         */
        case register = 1

        /**
         Type used to sign in.
         */
        case login = 2

        /**
         Type used to restore an existing account by reseting the password.
         */
        case forgotPassword = 3
    }


    // MARK: Constants

    static let defaultHomeServerUrl = "https://\(Config.defaultHomeServer)"
    static let defaultIdServerUrl = "https://\(Config.defaultIdServer)"


    // MARK: Public Properties

    public let homeServer: String

    public let idServer: String


    // MARK: Private Properties

    private var delegate: AuthenticationManagerDelegate?
    private let authType: AuthType

    private let client: MXRestClient
    private var currentOp: MXHTTPOperation?
    private var authSession: MXAuthenticationSession?

    private var username: String?
    private var password: String?
    private var recaptchaSecret: String?
    private var termsAccepted: Bool?
    private var useKeyBackup: Bool?
    private var keyBackupPassword: String?
    private var displayName: String?

    private var nextStage: MXLoginFlowType? {
        if let next = authSession?.flows?.first?.stages?.filter({ !(authSession?.completed?.contains($0) ?? false) }).first {
            return MXLoginFlowType(identifier: next)
        }

        return nil
    }


    // MARK: Initializers

    /**
     You will need to reinit, when `homeServer` or `idServer` was changed by the user!

     - parameter delegate: The delegate implementing the callbacks.
     - parameter authType: Currently, only `.register` and `.login` are supported!
     - parameter homeServer: The Matrix home server URL to use.
     - parameter idServer: The Matrix ID server URL to use.
     */
    public init(_ delegate: AuthenticationManagerDelegate, _ authType: AuthType, homeServer: URL, idServer: URL) {
        self.homeServer = homeServer.absoluteString
        self.idServer = idServer.absoluteString

        let origHomeServer = homeServer
        var homeServer: URL? = homeServer
        var idServer: URL? = idServer

        DomainHelper.shared.replaceWithAltDomain(homeServer: &homeServer, idServer: &idServer)

        self.delegate = delegate
        self.authType = authType
        client = MXRestClient(homeServer: homeServer ?? origHomeServer, unrecognizedCertificateHandler: nil)
        client.identityServer = idServer?.absoluteString

        let callback = opCompletion() { [weak self] (session: MXAuthenticationSession) in
            self?.authSession = self?.filterAuthSession(session)

            if self?.authSession == nil {
                self?.showUnsupportedError()
            }
        }

        if authType == .login {
            currentOp = client.getLoginSession(completion: callback)
        }
        else if authType == .register {
            currentOp = client.getRegisterSession(completion: callback)
        }
    }


    // MARK: Public Methods

    /**
     Start authentication process.

     - parameter username: The username to register or to log in with.
     - parameter password: The password to register for that account or to use for log in.
     - parameter recaptchaSecret: Acquired Recaptcha secret, if already done, or empty if not needed or not acquired, yet.
     - parameter termsAccepted: Flag, if user already accepted terms & conditions.
     - parameter useKeyBackup: Flag, if key backup should be set up after successful registration or log in.
     - parameter keyBackupPassword: Optional password used for key backup. If none given, account`password` will be used.
     - parameter displayName: The display name to set for the account. Optional. Use only when registering an account.
     */
    open func authenticate(_ username: String, _ password: String, _ recaptchaSecret: String,
                           _ termsAccepted: Bool, _ useKeyBackup: Bool,
                           _ keyBackupPassword: String?, _ displayName: String?)
    {
        self.username = nil
        self.password = nil
        self.recaptchaSecret = nil
        self.termsAccepted = nil
        self.useKeyBackup = nil
        self.keyBackupPassword = nil
        self.displayName = nil

        guard let nextStage = nextStage else {
            return
        }

        print("[\(String(describing: type(of: self)))] #authenticate: nextStage=\(nextStage)")

        var auth = [String: Any]()

        switch nextStage {
        case .password:
            auth["type"] = kMXLoginFlowTypePassword
            auth["identifier"] = [
                "type": kMXLoginIdentifierTypeUser,
                "user": username,
            ]
            auth["password"] = password

        case .recaptcha:
            if recaptchaSecret.isEmpty {
                showRecaptchaAndTerms(recaptchaSecret, termsAccepted)

                return
            }

            auth["type"] = kMXLoginFlowTypeRecaptcha
            auth["response"] = recaptchaSecret

        case .dummy:
            auth["type"] = kMXLoginFlowTypeDummy

        case .other(kMXLoginFlowTypeTerms):
            if !termsAccepted {
                showRecaptchaAndTerms(recaptchaSecret, termsAccepted)

                return
            }

            auth["type"] = kMXLoginFlowTypeTerms

        default:
            showUnsupportedError()

            return
        }

        self.username = username
        self.password = password
        self.recaptchaSecret = recaptchaSecret
        self.termsAccepted = termsAccepted
        self.useKeyBackup = useKeyBackup
        self.keyBackupPassword = keyBackupPassword
        self.displayName = displayName

        let callback = opCompletion() { (data: [String: Any]) in

            // For an unkown reason MXJSONModel.model(fromJSON:) isn't exposed to Swift anymore.
            if let loginResponse = MXLoginResponse.models(fromJSON: [data])?.first as? MXLoginResponse {
                self.onSuccesfulSignIn(MXCredentials(
                    loginResponse: loginResponse,
                    andDefaultCredentials: self.client.credentials))
            }
        }

        if authType == .login {
            currentOp?.cancel()

            if auth.keys.contains("password") {
                auth["initial_device_display_name"] = AuthenticationManager.generateDeviceName()
            }

            currentOp = client.login(parameters: auth, completion: callback)
        }
        else if authType == .register {
            currentOp?.cancel()

            auth["session"] = authSession?.session ?? ""

            let parameters: [String : Any] = [
                "auth": auth,
                "username": username,
                "password": password,
                "bind_email": false,
                "bind_msisdn": false,
                "initial_device_display_name": AuthenticationManager.generateDeviceName()
            ]

            currentOp = client.register(parameters: parameters, completion: callback)
        }
    }

    /**
     Proxy to `MXRestClient#isUserNameInUse`.
     */
    open func isUserNameInUse(_ username: String, _ completion: @escaping (Bool) -> Void) {
        client.isUserNameInUse(username, completion: completion)
    }

    /**
     Remove reference to delegate and cancel all pending operations.

     Don't use this class further after calling this!
     */
    open func close() {
        delegate?.showError(nil, false)
        delegate = nil

        currentOp?.cancel()
        authSession = nil
    }


    // MARK: Class Methods

    /**
     If the user enters only non-alpha characters, generate a random username.
     */
    open class func generateUserName() -> String {
        let username = Bundle.main.displayName + "-" + String.randomNumericString(length: 6)

        // Return filtered one (app name can contain illegal chars!)
        let allowedCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789=_-./"
        return username.filter { allowedCharacters.contains($0) }
    }

    /**
     We don't want to expose the device name (UIDevice.name) so generate one wich is leaking less information when registering with Matrix.

     - returns: Something like "Keanu iPhone".
     */
    open class func generateDeviceName() -> String {
        return "\(Bundle.main.displayName) \(UIDevice.current.model)"
    }


    // MARK: Private Methods

    /**
     Shows an error, explaining that this app doesn't support that server's authentication process.
     */
    private func showUnsupportedError() {
        let tx1: String

        if authType == .login {
            tx1 = "This app currently doesn't support this server's sign in process."
                .localize()
        }
        else {
            tx1 = "This app currently doesn't support this server's sign up process."
                .localize()
        }

        let tx2 = "Please choose another server!".localize()

        delegate?.showError("\(tx1)\n\(tx2)", true)
    }

    /**
     Encapsulates network request handling.

     - Shows "working" overlay as long as the request goes.
     - Displays errors to the user in a `LabelRow` above the username.
     - Calls through to your provided success callback, if there is no error
       and there actually is a returned object.
     - If there is a 401 error, that's most probably due to a multi-stage authentication, so if that happens,
       the next stage request is triggered instead of anything else.

     - parameter success: Callback in case of successful request.
     - parameter value: The data returned by the server.
     - returns: Completion callback suitable for some `MXRestClient` methods.
    */
    private func opCompletion<T>(_ success: @escaping (_ value: T) -> Void)
        -> ((_ response: MXResponse<T>) -> Void) {

        delegate?.showWorking(true)

        return { [weak self] response in
            self?.currentOp = nil

            let userInfo = (response.error as NSError?)?.userInfo

            // We get 401's when the authentication is multi-stage and we didn't complete all stages.
            if (userInfo?[AFNetworkingOperationFailingURLResponseErrorKey] as? HTTPURLResponse)?.statusCode == 401 {
                if let data = userInfo?[MXHTTPClientErrorResponseDataKey] as? [AnyHashable: Any],
                    let session = MXAuthenticationSession(fromJSON: data) {

                    self?.authSession = self?.filterAuthSession(session)

                    if self?.nextStage != nil,
                       let username = self?.username,
                       let password = self?.password,
                       let recaptchaSecret = self?.recaptchaSecret,
                       let termsAccepted = self?.termsAccepted,
                       let useKeyBackup = self?.useKeyBackup
                    {

                        // Do next stage.
                        DispatchQueue.main.async {
                            self?.authenticate(username, password, recaptchaSecret,
                                               termsAccepted, useKeyBackup,
                                               self?.keyBackupPassword, self?.displayName)
                        }

                        return
                    }
                }
            }

            self?.delegate?.showWorking(false)

            if response.isFailure || response.error != nil || response.value == nil {
                if response.error != nil {
                    self?.delegate?.showError(response.error?.localizedDescription, true)
                }
                else if response.value == nil {
                    self?.delegate?.showError("Server didn't return anything!".localize(), true)
                }
                else {
                    self?.delegate?.showError(nil, true)
                }
            }
            else {
                success(response.value!)
            }
        }
    }

    /**
     - Adds an account to the account manager, if not already in it.
     - Sets up cross signing and key backup.
     - Set the user's display name.
     - Informs the delegate about successful sign in.

     - parameter credentials: The `MXCredentials` as returned by the server.
     */
    private func onSuccesfulSignIn(_ credentials: MXCredentials) {
        print("[\(String(describing: type(of: self)))] credentials accessToken=\(credentials.accessToken ?? "nil") deviceId=\(credentials.deviceId ?? "nil") homeServer=\(credentials.homeServer ?? "nil") homeServerName=\(credentials.homeServerName() ?? "nil") userId=\(credentials.userId ?? "nil")")

        guard let manager = MXKAccountManager.shared() else {
            return // Something's very very wrong if this happens.
        }

        // Sanity check: check whether the user is not already logged in with this ID.
        var account: MXKAccount? = manager.account(forUserId: credentials.userId)
        account?.isDisabled = false // Existing account might be disabled.

        if account == nil {
            // Report the new account in account manager.
            account = MXKAccount(credentials: credentials)

            // Needs to be set immediately, otherwise app will crash after save of
            // MXKAccount, due to nil exception.
            account?.pushGatewayURL = PushManager.pushGatewayUrl

            if let account = account {
                account.identityServerURL = client.identityServer

                manager.addAccount(account, andOpenSession: true)
            }
        }

        if let account = account {
            // Need to wait for a session before setting cross signing and display name!
            account.performOnSessionReady { [weak self] session in
                if let password = self?.password {
                    // Opportunistically try to enable cross signing, if not done, yet.
                    // Fail silently, if not possible here.
                    // User will be asked to enable cross-signing on device verification, otherwise.
                    VerificationManager.shared.enableCrossSigning(session, password)
                }

                if self?.useKeyBackup ?? false,
                   let password = self?.keyBackupPassword ?? self?.password
                {
                    // Opportunistically try to restore the latest backup or create a new key backup,
                    // if not enabled, yet.
                    // The password is either a user-provided password which is an
                    // explicit password for the key backup, or, if none provided,
                    // the user account password.
                    // Fail silently, if not possible here.
                    // User needs to enable key backup in my-device scene, otherwise,
                    // or will automatically be connected to key backup, when doing
                    // an interactive verification with a device which already has it.
                    VerificationManager.shared.restoreOrAddNewBackup(session, password)
                }

                // Use whatever the user entered as display name.
                // (Should be filled on register, only.)
                if let displayName = self?.displayName, !displayName.isEmpty {
                    account.setUserDisplayName(displayName, success: nil, failure:nil)
                }
            }
        }

        delegate?.didLogIn()
    }

    private func showRecaptchaAndTerms(_ recaptchaSecret: String, _ termsAccepted: Bool) {
        let recaptchaKey = recaptchaSecret.isEmpty
            ? (authSession?.params?[kMXLoginFlowTypeRecaptcha] as? [String: String])?["public_key"]
            : nil

        let terms = termsAccepted
            ? nil
            : MXLoginTerms(fromJSON: authSession?.params[kMXLoginFlowTypeTerms] as? [AnyHashable : Any])

        delegate?.showRecaptchaAndTerms(client.homeserver, recaptchaKey, terms)
    }

    /**
     - parameter type: An array of `MXLoginFlowType`s as `String`s.
     - returns: if a given list of flow types is supported by this implementation or not.
     */
    private func isSupported(_ types: [String]?) -> Bool {
        if let types = types {
            for type in types {
                if !isSupported(type) {
                    return false
                }
            }

            return true
        }

        return false
    }

    /**
     - parameter type: A `MXLoginFlowType` as `String`.
     - returns: if a given flow type is supported by this implementation or not.
     */
    private func isSupported(_ type: String) -> Bool {
        switch MXLoginFlowType(identifier: type) {
        case .dummy, .password, .recaptcha, .other(kMXLoginFlowTypeTerms):
            return true
        default:
            print("[AuthenticationViewController] Login flow type \"\(type)\" is not supported.")
            return false
        }
    }

    /**
     - parameter authSession: An `MXAuthenticationSession` object, hopefully
     containing information about the login flows the server supports.
     - returns: `nil`, if no flow is supported or the server didn't provide a
     list of supported flows, the `authSession` object if all the flows are
     supported, or a copy of the `authSession` object which only contains the
     supported flows.
     */
    private func filterAuthSession(_ authSession: MXAuthenticationSession) -> MXAuthenticationSession? {

        var flows = [MXLoginFlow]()

        for flow in authSession.flows {
            if let type = flow.type {
                if isSupported(type) {
                    if flow.stages == nil || flow.stages.isEmpty {
                        flow.stages = [type]
                        flows.append(flow)
                    }
                    else {
                        if isSupported(flow.stages) {
                            flows.append(flow)
                        }
                    }
                }
            }
            else {
                if isSupported(flow.stages) {
                    flows.append(flow)
                }
            }
        }

        if flows.isEmpty {
            return nil
        }

        if flows.count == authSession.flows.count {
            return authSession
        }

        let cleanedSession = MXAuthenticationSession()
        cleanedSession.completed = authSession.completed
        cleanedSession.session = authSession.session
        cleanedSession.params = authSession.params
        cleanedSession.flows = flows

        return cleanedSession
    }
}
