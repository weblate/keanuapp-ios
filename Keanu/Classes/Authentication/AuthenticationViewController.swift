//
//  AuthenticationViewController.swift
//  Keanu
//
//  Created by Benjamin Erhart on 12.10.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import KeanuCore
import Eureka

public protocol AuthenticationViewControllerDelegate {

    func didLogIn(with userId: String)
}

open class AuthenticationViewController: FormViewController, AuthenticationManagerDelegate, RecaptchaAndTermsViewControllerDelegate {

    // MARK: Public Properties
    
    open var delegate: AuthenticationViewControllerDelegate?


    // MARK: Private Properties

    private var manager: AuthenticationManager?

    private lazy var workingOverlay: WorkingOverlay = {
        return WorkingOverlay().addToSuperview(view)
    }()

    private let authType: AuthenticationManager.AuthType

    private var recaptchaSecret = ""
    private var termsAccepted = false

    /**
     The username.

     This is basically a proxy for `form.rowBy(tag: "username")` which also trims.
     */
    private var username: String {
        get {
            if let username = (form.rowBy(tag: "username") as? AccountRow)?.value {
                let allowedCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789=_-./"
                var name = username.filter { allowedCharacters.contains($0) }
                if name.count == 0 {
                    // No characters left, generate a random one!
                    name = AuthenticationManager.generateUserName()
                }
                return name
            }

            return ""
        }
        set {
            if let username = form.rowBy(tag: "username") as? AccountRow {
                username.value = newValue
                username.reload()
            }
        }
    }

    /**
     Since we are trimming the user name, we remember what the user actually entered and use that as the display name for the account.
     */
    public var userEnteredName: String? {
        return (form.rowBy(tag: "username") as? AccountRow)?.value?.trimmingCharacters(in: .whitespacesAndNewlines)
    }

    /**
     The password.

     This is a proxy for `form.rowBy(tag: "password")`.
     */
    private var password: String {
        get {
            if let password = (form.rowBy(tag: "password") as? PasswordRow)?.value {
                return password
            }

            return ""
        }
        set {
            if let password = form.rowBy(tag: "password") as? PasswordRow {
                password.value = newValue
                password.reload()
            }
        }
    }

    private var homeServer: URL {
        get {
            if let homeServer = (form.rowBy(tag: "home_server") as? URLRow)?.value {
                return homeServer
            }

            return URL(string: AuthenticationManager.defaultHomeServerUrl)!
        }
        set {
            if let homeServer = form.rowBy(tag: "home_server") as? URLRow {
                homeServer.value = newValue
                homeServer.reload()
            }
        }
    }

    private var idServer: URL {
        get {
            if let idServer = (form.rowBy(tag: "id_server") as? URLRow)?.value {
                return idServer
            }

            return URL(string: AuthenticationManager.defaultIdServerUrl)!
        }
        set {
            if let idServer = form.rowBy(tag: "id_server") as? URLRow {
                idServer.value = newValue
                idServer.reload()
            }
        }
    }

    private var useKeyBackup: Bool {
        get {
            return (form.rowBy(tag: "key_backup") as? SwitchRow)?.value ?? true
        }
        set {
            if let useKeyBackup = form.rowBy(tag: "key_backup") as? SwitchRow {
                useKeyBackup.value = newValue
                useKeyBackup.reload()
            }
        }
    }

    /**
     The key backup password provided by the user. Will return `nil` *also*, if actually just empty.
     */
    private var keyBackupPassword: String? {
        get {
            if let password = (form.rowBy(tag: "key_backup_password") as? PasswordRow)?.value,
                !password.isEmpty
            {
                return password
            }

            return nil
        }
        set {
            if let password = form.rowBy(tag: "key_backup_password") as? PasswordRow {
                password.value = newValue
                password.reload()
            }
        }
    }

    private let checkRowValidity = { (cell: BaseCell, row: BaseRow) in
        if #available(iOS 13.0, *) {
            // Handle dark mode
            cell.backgroundColor = row.isValid ? .secondarySystemGroupedBackground : .orange
        }
        else {
            cell.backgroundColor = row.isValid ? .white : .orange
        }
    }


    public init(_ type: AuthenticationManager.AuthType) {
        self.authType = type

        super.init(style: .grouped)
    }

    required public init?(coder: NSCoder) {
        authType = coder.decodeObject(forKey: "type") as? AuthenticationManager.AuthType ?? .login

        super.init(coder: coder)
    }

    open override func encode(with coder: NSCoder) {
        coder.encode(authType, forKey: "type")

        super.encode(with: coder)
    }


    // MARK: UIViewController

    open override func viewDidLoad() {
        super.viewDidLoad()

        form
        +++ Section()

            <<< LabelRow("errors") {
                $0.hidden = true
            }
            .cellUpdate() { cell, row in
                cell.textLabel?.textColor = .orange
                cell.textLabel?.numberOfLines = 0
            }

            <<< AccountRow("username") {
                $0.placeholder = "User Name".localize()
                $0.add(rule: RuleRequired())
            }
            .onCellHighlightChanged() { _, row in
                // Check username availability on registration.
                if self.authType == .register {
                    if !self.username.isEmpty {
                        self.manager?.isUserNameInUse(self.username) { used in
                            if used {
                                self.showError("This username is already taken.".localize())
                            }
                            else {
                                self.showError(nil, false)
                            }
                        }
                    }
                }
            }
            .onRowValidationChanged(checkRowValidity)

        if authType == .register {
            form.first!.footer = HeaderFooterView(stringLiteral:
                "Think of a unique nickname that you don't use anywhere else and doesn't contain personal information."
                    .localize())

            form
            +++ Section(footer:
                "Make sure your password is a unique password you don't use anywhere else."
                    .localize())
        }

        form.last!
            <<< PasswordRow("password") {
                $0.placeholder = "Password".localize()
                $0.add(rule: RuleRequired())

                if authType == .register {
                    $0.add(rule: RuleMinLength(minLength: 8))
                }
            }
            .cellSetup() { cell, row in
                cell.accessoryType = .detailButton
            }
            .onRowValidationChanged(checkRowValidity)

        if authType == .register {
            form.last!
                <<< PasswordRow("confirm") {
                    $0.placeholder = "Confirm".localize()
                    $0.add(rule: RuleRequired())
                    $0.add(rule: RuleEqualsToRow(form: form, tag: "password"))
                    $0.add(rule: RuleMinLength(minLength: 8))
                }
                .cellSetup() { cell, row in
                    cell.accessoryType = .detailButton
                }
                .onRowValidationChanged(checkRowValidity)
        }

        form
        +++ Section()
        <<< SwitchRow("advanced_options") {
                $0.title = "Show Advanced Options".localize()
                $0.value = false
            }
            .onChange() { _ in self.restartSession() }

        form
        +++ Section("Server".localize()) {
            $0.hidden = "$advanced_options == false"
        }
            <<< URLRow("home_server") {
                $0.title = "Home Server".localize()
                $0.placeholder = AuthenticationManager.defaultHomeServerUrl
                $0.value = URL(string: AuthenticationManager.defaultHomeServerUrl)
                $0.add(rule: RuleURL())
            }
            .onCellHighlightChanged() { _,_  in self.restartSession() }

            <<< SwitchRow("customIdServer") {
                $0.title = "Use Custom Identity Server".localize()
                $0.value = false
            }

            <<< URLRow("id_server") {
                $0.title = "Identity Server".localize()
                $0.placeholder = AuthenticationManager.defaultIdServerUrl
                $0.value = URL(string: AuthenticationManager.defaultIdServerUrl)
                $0.hidden = "$customIdServer == false"
                $0.add(rule: RuleURL())
            }
            .onCellHighlightChanged() { _,_  in self.restartSession() }

        +++ Section(header: "Key Backup".localize(),
                    footer: "Our secure server remembers your encryption keys for you, so you can view your message history on all of your devices. Your account password is used unless you set a custom one.".localize()
        ) {
            $0.hidden = "$advanced_options == false"
        }

        <<< SwitchRow("key_backup") {
            $0.title = "Key Backup".localize()
            $0.value = true
        }

        <<< SwitchRow("own_key_backup_password") {
            $0.title = "Choose Password".localize()
            $0.value = false
            $0.hidden = "$advanced_options == false || $key_backup == false"
        }

        <<< PasswordRow("key_backup_password") {
            $0.placeholder = "Key Backup Password".localize()
            $0.hidden = "$advanced_options == false || $key_backup == false || $own_key_backup_password == false"

            if authType == .register {
                $0.add(rule: RuleMinLength(minLength: 8))
            }
        }
        .cellSetup() { cell, row in
            cell.accessoryType = .detailButton
        }
        .onRowValidationChanged(checkRowValidity)

        <<< PasswordRow("key_backup_confirm") {
            $0.placeholder = "Confirm".localize()
            $0.hidden = "$advanced_options == false || $key_backup == false || $own_key_backup_password == false"
            $0.add(rule: RuleEqualsToRow(form: form, tag: "key_backup_password"))

            if authType == .register {
                $0.add(rule: RuleMinLength(minLength: 8))
            }
        }
        .cellSetup() { cell, row in
            cell.accessoryType = .detailButton
        }
        .onRowValidationChanged(checkRowValidity)

        if authType == .register {
            navigationItem.title = "Sign Up".localize()
        } else {
            navigationItem.title = "Sign In".localize()
        }

        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done,
                                                            target: self,
                                                            action: #selector(onDone))
    }

    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        navigationController?.setNavigationBarHidden(false, animated: animated)

        restartSession()
    }

    open override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        // Resolve circular reference.
        manager?.close()
        manager = nil
    }
    

    // MARK: UITableViewDelegate

    /**
     The accessory button should only be on the password fields, so we assume that one was tapped.

     Toggles password visibility on tap.
     */
    func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? PasswordCell {
            cell.textField.isSecureTextEntry = !cell.textField.isSecureTextEntry
        }
    }


    // MARK: AuthenticationManagerDelegate

    public func showWorking(_ toggle: Bool) {
        workingOverlay.isHidden = !toggle
    }

    /**
     Shows `RecaptchaAndTermsViewController` modaly.
    */
    public func showRecaptchaAndTerms(_ homeServer: String, _ recaptchaKey: String?, _ terms: MXLoginTerms?) {
        present(RecaptchaAndTermsViewController(homeServer, recaptchaKey, terms, delegate: self), animated: true)
    }

    /**
     Shows an error text to the user, if `toggle` is true.

     - parameter error: The error text to show. If `nil`, a standard text will
        be displayed. Default is `nil`.
     - parameter toggle: Show if true, hide if false. Default is `true`.
     */
    open func showError(_ error: String? = nil, _ toggle: Bool = true) {
        if let errors = form.rowBy(tag: "errors") {
            if toggle {
                errors.title = error ?? "No details available.".localize()
                errors.reload()
                errors.hidden = false
            }
            else {
                errors.hidden = true
            }

            errors.evaluateHidden()
        }
    }

    open func didLogIn() {
        delegate?.didLogIn(with: username)
    }


    // MARK: RecaptchaAndTermsViewControllerDelegate

    /**
     User returned successfully from the Recaptcha solving and terms accepting.

     We can directly progress with sign in/up.

     - parameter secret: The secret from the solved Recaptcha.
    */
    open func success(_ secret: String?) {
        recaptchaSecret = secret ?? ""
        termsAccepted = true

        onDone()
    }


    // MARK: Actions

    /**
     The user pressed the "Sign In"/"Sign Up" button, which should have been
     possible only, if it is enabled because all credential fields are
     sufficiently filled.
    */
    @objc func onDone() {
        guard form.validate().isEmpty else {
            return
        }

        manager?.authenticate(username, password, recaptchaSecret, termsAccepted, useKeyBackup, keyBackupPassword, userEnteredName)
    }


    // MARK: Private Methods

    /**
     Starts a new session with the currently set home server.

     You should call this on scene appearance or when the user changes the
     home server.
    */
    private func restartSession() {
        if manager == nil || homeServer.absoluteString != manager?.homeServer || idServer.absoluteString != manager?.idServer {
            manager?.close()
            manager = nil

            manager = AuthenticationManager(self, authType, homeServer: homeServer, idServer: idServer)
        }
    }
}
