#!/usr/bin/env sh

# Get absolute path to this script.
dir=$(cd `dirname $0` && pwd)

# 1. List all currently used MXK files in the Keanu iOS project.
# 2. Find files with the same name in the matrix-ios-kit project.
# 3. Copy all of them to the Keanu iOS project.
ls $dir/../../KeanuCore/Classes/MatrixKit/ \
| xargs -L 1 -I {} find $dir/../../../matrix-ios-kit/ -name {} -print0 \
| \xargs -0 -I {} cp -a {} $dir/../../KeanuCore/Classes/MatrixKit/

# Delete the old MatrixKit strings.
rm -r $dir/../../KeanuCore/Assets/MatrixKitAssets.bundle

# Take in the fresh ones.
cp -a $dir/../../../matrix-ios-kit/MatrixKit/Assets/MatrixKitAssets.bundle \
$dir/../../KeanuCore/Assets/
