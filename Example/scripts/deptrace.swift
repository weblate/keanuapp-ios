#!/usr/bin/env xcrun --sdk macosx swift

//
//  deptrace.swift
//  Keanu
//
//  Created by Benjamin Erhart on 27.03.20.
//  Copyright © 2020 Guardian Project. All rights reserved.
//

import Foundation


// MARK: Config

let root = resolve("../../../matrix-ios-kit/MatrixKit/")

let target = resolve("../../KeanuCore/Classes/MatrixKit")

let files = [
    "MXKAttachment.h",
    "MXKAttachment.m",
    "MXKAccount.h",
    "MXKAccount.m",
    "MXKAccountManager.h",
    "MXKAccountManager.m",
    "MXKAuthenticationRecaptchaWebView.h",
    "MXKAuthenticationRecaptchaWebView.m",

    "MXAggregatedReactions+MatrixKit.h",
    "MXAggregatedReactions+MatrixKit.m",
    "MXEvent+MatrixKit.h",
    "MXEvent+MatrixKit.m",
    "MXKAppSettings.h",
    "MXKAppSettings.m",
    "MXKCellData.h",
    "MXKCellData.m",
    "MXKCellRendering.h",
    "MXKCellRendering.m",
    "MXKConstants.h",
    "MXKConstants.m",
    "MXKDataSource.h",
    "MXKDataSource.m",
    "MXKEventFormatter.h",
    "MXKEventFormatter.m",
    "MXKImageView.h",
    "MXKImageView.m",
    "MXKMessageTextView.h",
    "MXKMessageTextView.m",
    "MXKPieChartView.h",
    "MXKPieChartView.m",
    "MXKQueuedEvent.h",
    "MXKQueuedEvent.m",
    "MXKReceiptSendersContainer.h",
    "MXKReceiptSendersContainer.m",
    "MXKResponderRageShaking.h",
    "MXKResponderRageShaking.m",
    "MXKRoomBubbleCellData.h",
    "MXKRoomBubbleCellData.m",
    "MXKRoomBubbleCellDataStoring.h",
    "MXKRoomBubbleCellDataStoring.m",
    "MXKRoomBubbleComponent.h",
    "MXKRoomBubbleComponent.m",
    "MXKRoomBubbleTableViewCell.h",
    "MXKRoomBubbleTableViewCell.m",
    "MXKRoomDataSource.h",
    "MXKRoomDataSource.m",
    "MXKRoomDataSourceManager.h",
    "MXKRoomDataSourceManager.m",
    "MXKRoomNameStringLocalizations.h",
    "MXKRoomNameStringLocalizations.m",
    "MXKSendReplyEventStringLocalizations.h",
    "MXKSendReplyEventStringLocalizations.m",
    "MXKSlashCommands.h",
    "MXKSlashCommands.m",
    "MXKTableViewCell.h",
    "MXKTableViewCell.m",
    "MXKTools.h",
    "MXKTools.m",
    "MXKView.h",
    "MXKView.m",
    "MXKViewController.h",
    "MXKViewController.m",
    "MXKViewControllerHandling.h",
    "MXKViewControllerHandling.m",
    "MXRoom+Sync.h",
    "MXRoom+Sync.m",
    "NSBundle+MXKLanguage.h",
    "NSBundle+MXKLanguage.m",
    "NSBundle+MatrixKit.h",
    "NSBundle+MatrixKit.m",
    "UITextView+MatrixKit.h",
    "UITextView+MatrixKit.m",
    "UIViewController+MatrixKit.h",
    "UIViewController+MatrixKit.m",
]

let regex = try? NSRegularExpression(pattern: "[#@](include|import)", options: .caseInsensitive)

// MARK: Helper Methods

func exit(_ msg: String) {
    print(msg)
    exit(1)
}

func resolve(_ path: String) -> URL {
    let cwd = URL(fileURLWithPath: FileManager.default.currentDirectoryPath)
    let script = URL(fileURLWithPath: CommandLine.arguments.first ?? "", relativeTo: cwd).deletingLastPathComponent()

    return URL(fileURLWithPath: path, relativeTo: script)
}


// MARK: Main

let fm = FileManager.default

let enumerator = fm.enumerator(at: root, includingPropertiesForKeys: [.isDirectoryKey], options: []) { url, error in
    print(error.localizedDescription)

    return true
}

var result = [String: Bool]()

do {
    try fm.createDirectory(at: target, withIntermediateDirectories: true)
}
catch {
    print(error.localizedDescription)
    exit(1)
}

while let file = enumerator?.nextObject() as? URL {
    if (try? file.resourceValues(forKeys: [.isDirectoryKey]).isDirectory) ?? false {
        continue
    }

    if !files.contains(file.lastPathComponent) {
        continue
    }

    if let content = try? String(contentsOf: file) {
        for line in content.split(separator: "\n") {
            let line = String(line)

            if regex?.firstMatch(in: line, options: [], range: NSRange(line.startIndex ..< line.endIndex, in: line))?.numberOfRanges ?? 0 > 0 {
                result[line] = true
            }
        }
    }

    do {
        let to = target.appendingPathComponent(file.lastPathComponent)

//        print("Copy \(file.path) to \(to.path)")
        try fm.copyItem(at: file, to: to)
    }
    catch {
        print(error.localizedDescription)
    }
}

for line in result.keys.sorted() {
    print(line)
}


// Wait on explicit exit.
//_ = DispatchSemaphore(value: 0).wait(timeout: .distantFuture)
