//
//  NSAttributedString+Keanu.swift
//  KeanuCore
//
//  Created by Benjamin Erhart on 31.07.20.
//

import Foundation

extension NSAttributedString {

    /**
     Creates an attributed string with a bold label and regular content.

     - parameter string: The complete string, including the label and the content.
     - parameter onlyContent: The content again, but just that, to find the range of it.
     - parameter size: The font size to use. Defaults to `UIFont.systemFontSize`.
     - returns: An attributed string, everything in bold system font, except the content as regular system font.
     */
    open class func withBoldLabel(_ string: String, onlyContent: String, size: CGFloat = UIFont.systemFontSize) -> NSAttributedString {
        let text = NSMutableAttributedString(
            string: string, attributes: [.font: UIFont.boldSystemFont(ofSize: size)])

        if let range = string.range(of: onlyContent) {
            text.setAttributes([.font: UIFont.systemFont(ofSize: size)], range: NSRange(range, in: string))
        }

        return text
    }

    /**
     Creates proper formatting for a device key fingerprint.

     Copious amounts of letter and line spacing are used.

     - parameter text: Falls back to an error message, if nil.
     - parameter size: The font size to use. Defaults to `UIFont.systemFontSize`.
     - parameter alignment: Alignment used, defaults to `natural`.
     - parameter bold: If bold system font should be used. Defaults to `true`.
     - returns: An `NSAttributedString` containing the text, formatted as a very readable key fingerprint.
     */
    open class func fingerprint(_ text: String?, size: CGFloat = UIFont.systemFontSize,
                                alignment: NSTextAlignment = .natural, bold: Bool = true) -> NSAttributedString
    {
        let style = NSMutableParagraphStyle()
        style.lineSpacing = 5
        style.alignment = alignment

        var attributes: [NSAttributedString.Key : Any] = [.kern: 1, .paragraphStyle: style]

        if bold {
            attributes[.font] = UIFont.boldSystemFont(ofSize: size)
        }

        let string = text?.split(by: 4).joined(separator: " ")
            // Error message instead of key fingerprint which could not be read.
            ?? "CODE ERROR".localize()

        return NSAttributedString(string: string, attributes: attributes)
    }
}
