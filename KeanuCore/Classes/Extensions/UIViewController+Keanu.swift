//
//  UIViewController+Keanu.swift
//  AFNetworking
//
//  Created by N-Pex on 07.05.19.
//

import Foundation

extension UIViewController {

    public var top: UIViewController {
        if let vc = subViewController {
            return vc.top
        }

        return self
    }

    public var subViewController: UIViewController? {
        if let vc = self as? UINavigationController {
            return vc.topViewController
        }

        if let vc = self as? UISplitViewController {
            return vc.viewControllers.last
        }

        if let vc = self as? UITabBarController {
            return vc.selectedViewController
        }

        if let vc = presentedViewController {
            return vc
        }

        return nil
    }

    /**
     Adds a `UITapGestureRecognizer` to the root `UIView` which hides the
     soft keyboard, if one is shown, so users can hide the keyboard this way.
     */
    public func hideKeyboardOnOutsideTap() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    /**
     Hides the soft keyboard, if one is shown.
     */
    @objc public func dismissKeyboard() {
        view.endEditing(true)
    }
}
