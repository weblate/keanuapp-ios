//
//  MXRoomSummary+Keanu.swift
//  Keanu
//
//  Created by N-Pex on 26.09.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import MatrixSDK

extension MXRoomSummary {

    public var isInvite: Bool {
        get {
            return membership == .invite
        }
    }
    
    public var isArchived: Bool {
        get {
            return (others["isArchive"] as? NSNumber)?.boolValue ?? false
        }
        set {
            others["isArchive"] = newValue ? NSNumber(booleanLiteral: true) : nil

            save(true)
        }
    }

    public var canonicalAlias: String? {
        get {
            return others["canonicalAlias"] as? String
        }
        set {
            others["canonicalAlias"] = newValue
        }
    }

    public func updateCanonicalAlias(_ completion: ((_ success: Bool) -> Void)? = nil) {
        mxSession?.matrixRestClient?.canonicalAlias(ofRoom: roomId) { [weak self] response in
            self?.canonicalAlias = response.value

            completion?(response.isSuccess)
        }
    }

    /**
     Return a formatted version of the last message.

     Clients can set a static delegate to do the actual formatting. See `lastMessageFormatter`.

     - parameter callback: A callback receiving the formatted message.
     - parameter message: The formatted message.
     */
    public func getLastMessageFormatted(_ callback: @escaping (_ message: String?) -> Void) {
        guard let lastEventId = lastMessage?.eventId else {
            return callback(lastMessage?.text)
        }

        // Fetch last event and decrypt, if necessary.
        mxSession?.event(withEventId: lastEventId, inRoom: roomId, success: { event in
            guard let event = event else {
                return callback(self.lastMessage?.text)
            }

            // Avoid showing "unable to decrypt" if we can't show it.
            if event.isEncrypedAndSentBeforeWeJoined() {
                return callback(nil)
            }

            // If a result is returned from the formatter, use that. Otherwise use default handling.
            if let result = MXRoomSummary.lastMessageFormatter?(event) {
                return callback(result)
            }

            if event.isMediaAttachment(),
               let attachment = MXKAttachment(event: event, andMediaManager: self.mxSession.mediaManager)
            {
                if attachment.type == MXKAttachmentTypeVideo {
                    return callback("Video file".localize())
                }
                else if attachment.type == MXKAttachmentTypeImage {
                    return callback("Image file".localize())
                }
                else if event.isPdfAttachment {
                    return callback("PDF".localize())
                }
                else if let mimeType = attachment.contentInfo?["mimetype"] as? String,
                        mimeType.starts(with: "audio/")
                {
                    if mimeType.contains("x-m4a") {
                        return callback("Voice message".localize())
                    }
                    else {
                        return callback("Audio file".localize())
                    }
                }
                else if attachment.type == MXKAttachmentTypeFile {
                    return callback(attachment.fileDisplayName ?? self.lastMessage?.text)
                }
            }

            if let session = self.mxSession, let formatter = session.roomSummaryUpdateDelegate as? MXKEventFormatter {
                self.room.state { state in
                    if let state = state {
                        let typedPointer = UnsafeMutablePointer<MXKEventFormatterError>.allocate(capacity: 1)
                        callback(formatter.string(from: event, with: state, error: typedPointer))
                    } else {
                        callback(self.lastMessage?.text)
                    }
                }
            } else {
                callback(self.lastMessage?.text)
            }
        },
        failure: { error in
            print("[\(String(describing: type(of: self)))]#getLastMessageFormatted Error: \(error?.localizedDescription ?? "nil")")

            callback(self.lastMessage?.text)
        })
    }
    
    private struct AssociatedKeys {
        static var lastMessageFormatter = "lastMessageFormatter"
    }

    /**
     Set a formatter to use for formatting the "last message", e.g. in a list of chats. If this callback returns a non-nil value, then use that. Otherwise, fall back on default functionality.
     */
    public static var lastMessageFormatter: ((MXEvent) -> String?)? {
        get {
            return objc_getAssociatedObject(self, &AssociatedKeys.lastMessageFormatter) as? ((MXEvent) -> String?)
        }
        set(newValue) {
            guard let newValue = newValue else { return }
            objc_setAssociatedObject(self, &AssociatedKeys.lastMessageFormatter, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
}
