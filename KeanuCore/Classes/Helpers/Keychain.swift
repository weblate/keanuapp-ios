//
//  Keychain.swift
//  KeanuCore
//
//  Created by Benjamin Erhart on 25.06.20.
//

import Foundation

open class Keychain {

    /**
     Holds all essential data of a credentials item.
     */
    public struct Credentials {
        public var server: String?
        public var username: String?
        public var password: String?
        public var description: String?
        public var comment: String?

        /**
         General constructor.

         - parameter server: The Matrix home server.
         - parameter username: The user part of a Matrix ID.
         - parameter password: The password to store.
         - parameter description: A user readable description. (In theory: We can't store to the user-readable keychain, just an app-private keychain.)
         - parameter comment: A user editable comment. (In theory: We can't store to the user-readable keychain, just an app-private keychain.)
         */
        public init(server: String? = nil, username: String? = nil, password: String? = nil, description: String? = nil, comment: String? = nil)
        {
            self.server = server
            self.username = username
            self.password = password
            self.description = description
            self.comment = comment
        }

        /**
         Constructor for a `SecItemCopyMatching` output.

         - parameter item: Output of a `SecItemCopyMatching` call.
         */
        public init(_ item: [CFString: Any])
        {
            server = item[kSecAttrServer] as? String
            username = item[kSecAttrAccount] as? String

            if let passwordData = item[kSecValueData] as? Data {
                password = String(data: passwordData, encoding: .utf8)
            }

            description = item[kSecAttrDescription] as? String
            comment = item[kSecAttrComment] as? String
        }

        /**
         Construct a query or a recovery key credentials item from a Matrix ID.

         - parameter matrixId: A full Matrix ID, including username and server.
         - parameter recoveryKey: Optional. If not provided, credentials are good as a query.
         */
        public init(matrixId: String, _ recoveryKey: String? = nil)
        {
            self.init(
                server: MXTools.serverName(inMatrixIdentifier: matrixId),
                username: matrixId.components(separatedBy: ":").first?.replacingOccurrences(of: "@", with: ""),
                password: recoveryKey,
                description: "Matrix Key Backup Recovery Key")
        }

        /**
         - returns: A query dictionary good as input for `SecItemAdd`, `SecItemCopyMatching` or similar calls.
         */
        public func asQuery() -> [CFString: Any]
        {
            var query: [CFString: Any] = [kSecClass: kSecClassGenericPassword]

            if let server = server as CFString? {
                query[kSecClass] = kSecClassInternetPassword
                query[kSecAttrServer] = server
            }

            if let username = username as CFString? {
                query[kSecAttrAccount] = username
            }

            if let password = password?.data(using: .utf8) as CFData? {
                query[kSecValueData] = password
            }

            if let description = description as CFString? {
                query[kSecAttrDescription] = description
            }

            if let comment = comment as CFString? {
                query[kSecAttrComment] = comment
            }

            return query
        }
    }

    enum KeychainError: Error {
        case store(status: OSStatus)
        case read(status: OSStatus)
    }

    /**
     Store credentials item to the app-private keychain.

     - parameter credentials: The credentials to add.
     - throws: `KeychainError.store` in case of failure.
     */
    open class func store(_ credentials: Credentials) throws
    {
        let status = SecItemAdd(credentials.asQuery() as CFDictionary, nil)

        guard status == errSecSuccess else {
            throw KeychainError.store(status: status)
        }

// Currently, we store to an app-private keychain. Users can't see these items in
// iOS' Settings app or the MacOS Keychain Access app. (Even though they should get
// synchronized in the cloud.)
//
// The only other way to achieve this, is using "Shared Web Credentials", but these
// are only good for associated domains. No others are allowed there, and they're
// also not really the right thing to use for Matrix' key backup recovery key.
//
// Anyway, here's some code how to achieve that:
//
//        if let server = credentials.server as CFString?,
//            let username = credentials.username as CFString?
//        {
//            SecAddSharedWebCredential(server, username, credentials.password as CFString?) { error in
//                print("[\(String(describing: type(of: self)))]#store error=\(error)")
//            }
//        }
    }

    /**
     Read the first credentials item from the keychain, which matches the given search query.

     - parameter credentials: The search query. (Probably all credentials minus the password.)
     - throws: `KeychainError.read` in case of failure.
     */
    open class func read(_ credentials: Credentials) throws -> Credentials?
    {
        var item: CFTypeRef?

        var query = credentials.asQuery()
        query[kSecReturnAttributes] = kCFBooleanTrue
        query[kSecReturnData] = kCFBooleanTrue

        let status = SecItemCopyMatching(query as CFDictionary, &item)

        guard status == errSecSuccess else {
            throw KeychainError.read(status: status)
        }

        if let item = item as? [CFString: Any] {
            return Credentials(item)
        }

        return nil
    }

    /**
     Delete all credentials from the keychain matching the given query.

     - parameter credentials: The search query. (Probably all credentials minus the password.)
     - throws: `KeychainError.store` in case of failure.
     */
    open class func delete(_ credentials: Credentials) throws
    {
        let status = SecItemDelete(credentials.asQuery() as CFDictionary)

        guard status == errSecSuccess else {
            throw KeychainError.store(status: status)
        }
    }
}
